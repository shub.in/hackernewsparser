from typing import List

from app.schemas.errors import Error
from app.schemas.hackernews import HackernewsOut, HackernewsFilter, HackernewsStatusResponse
from app.models.hackernews import HackernewsModel
from app.queue import queues


class HackernewsCRUD:
    @staticmethod
    async def list(filter: HackernewsFilter = HackernewsFilter()) -> (List[HackernewsOut], Error):
        items = await HackernewsModel.list(filter=filter)
        return items, None

    @staticmethod
    async def parse() -> (HackernewsStatusResponse, Error):
        await queues.parsing_worker.put(None)
        return HackernewsStatusResponse(status='ok'), None
