import asyncio
import logging

from app.config import config
from app.parsers.hackernews import HackerNewsSpider
from app.queue import queues


async def parsing_worker():
    logging.info('Parsing worker started')
    await asyncio.sleep(5)
    while True:
        await asyncio.wait([
            asyncio.sleep(config.parsing_interval),
            queues.parsing_worker.get()
        ], return_when=asyncio.FIRST_COMPLETED)
        await HackerNewsSpider.async_start(loop=asyncio.get_running_loop(), cancel_tasks=False)
