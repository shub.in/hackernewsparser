from fastapi import APIRouter

from . import hackernews

router = APIRouter()
router.include_router(hackernews.router, prefix='', tags=['hackernews'])
