from typing import List

from fastapi import APIRouter, HTTPException, Query, Depends, BackgroundTasks
from starlette import status

from app.schemas.errors import Error
from app.schemas.hackernews import (HackernewsOut, HackernewsFilter, HackernewsOrderFields, HackernewsDefaults,
                                    get_order_field_and_is_asc, HackernewsStatusResponse)
from app.crud.hackernews import HackernewsCRUD

router = APIRouter()


async def hackernews_filter(
        order: HackernewsOrderFields = Query(default=HackernewsOrderFields.created_desc,
                                             description='Option with "-" symbol means descending.'),
        offset: int = Query(default=HackernewsDefaults.OFFSET, ge=HackernewsDefaults.OFFSET_MIN_GE,
                            description=f'Offset value should be great or equal {HackernewsDefaults.OFFSET_MIN_GE}'),
        limit: int = Query(default=HackernewsDefaults.LIMIT, ge=HackernewsDefaults.LIMIT_MIN_GE,
                           le=HackernewsDefaults.LIMIT_MAX_LE,
                           description=f'Limit value should be from {HackernewsDefaults.LIMIT_MIN_GE} to '
                                       f'{HackernewsDefaults.LIMIT_MAX_LE}')):
    field, is_asc = get_order_field_and_is_asc(order)
    return HackernewsFilter(order=field, is_asc=is_asc, offset=offset, limit=limit)


@router.get('/posts',
            status_code=status.HTTP_200_OK,
            response_model=List[HackernewsOut],
            responses={500: {'model': Error}})
async def list(filter: HackernewsFilter = Depends(hackernews_filter)):
    response, error = await HackernewsCRUD.list(filter=filter)
    if error is not None:
        raise HTTPException(detail=error.detail, status_code=error.status_code)
    return response


@router.post('/parse',
             status_code=status.HTTP_200_OK,
             response_model=HackernewsStatusResponse)
async def parse():
    response, error = await HackernewsCRUD.parse()
    if error is not None:
        raise HTTPException(detail=error.detail, status_code=error.status_code)
    return response

