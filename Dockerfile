FROM python:3.7-slim
WORKDIR /dbilling
RUN apt-get update && apt-get install -y gcc build-essential
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .
CMD uvicorn app:app --host 0.0.0.0
