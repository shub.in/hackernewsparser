import uvicorn
from app.main import make_app
from app.config import config


if __name__ == '__main__':
    uvicorn.run(app=make_app(config), host=config.host, port=config.port)
