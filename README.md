# Hacker News Parser

### Setup & Run

You need a [config](#config-example) for run application

**Local:**
```bash
git clone git@gitlab.com:shub.in/hackernewsparser.git
cd hackernewsparser
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
docker-compose run -d db
alembic upgrade head
python3 run.py
```

**Docker Compose:**
```bash
docker-compose up -d db
docker-compose build app                    # Only for first time run
docker-compose run app alembic upgrade head # Only for first time run
docker-compose run app pytest               # For run tests
docker-compose up app
```

### Config example

```yaml
db: postgresql://postgres:postgres@localhost:5432/postgres # Host should be "db" instead of "localhost" for docker-compose
host: 0.0.0.0
port: 8000
version: 0.1.0
parsing_interval: 600 # In seconds
#api_prefix: /api/hackernews
```

### Migrations

```bash
alembic revision --autogenerate -m "Migration description"
alembic upgrade head
```

### Tests

```bash
pytest
```

### Helpful docs

* [FastAPI](https://fastapi.tiangolo.com/) (Web Framework)
* [Alembic](https://alembic.sqlalchemy.org) (Database migrations)
* [Databases](https://www.encode.io/databases/)
