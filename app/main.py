import asyncio

from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware

from .config import Config
from .database import db
from .routers import router
from .workers.parsing import parsing_worker
from .queue import queues


async def startup():
    await db.connection.connect()
    queues.parsing_worker = asyncio.Queue()


async def background():
    asyncio.create_task(parsing_worker())


async def shutdown():
    await db.connection.disconnect()


def make_app(config: Config) -> FastAPI:
    api_prefix = config.api_prefix
    app = FastAPI(title='Hacker News Parser', version=config.version,
                  openapi_url=f'{api_prefix}/openapi.json' if config.docs else None,
                  docs_url=f'{api_prefix}/docs' if config.docs else None,
                  redoc_url=f'{api_prefix}/redoc' if config.docs else None)
    app.add_middleware(CORSMiddleware,
                       allow_credentials=True,
                       allow_origins=['*'],
                       allow_methods=['*'],
                       allow_headers=['*'])
    app.add_event_handler('startup', startup)
    app.add_event_handler('startup', background)
    app.add_event_handler('shutdown', shutdown)
    app.include_router(router, prefix=config.api_prefix)

    return app
