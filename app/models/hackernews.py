from datetime import datetime
from typing import List

from sqlalchemy import Column, String, Table, DateTime, Integer, desc, asc

from app.database import db
from app.schemas.hackernews import HackernewsIn, HackernewsOut, HackernewsFilter
from app.schemas.errors import Error

hackernews = Table(
    'hackernews',
    db.metadata,
    Column('id', Integer, primary_key=True),
    Column('title', String, nullable=False),
    Column('url', String, nullable=False),
    Column('created', DateTime(timezone=True), nullable=False, default=datetime.utcnow),
)


class HackernewsModel:
    @staticmethod
    async def insert_or_pass(item: HackernewsIn) -> Error or None:
        query = hackernews.select().where(hackernews.c.id == item.id)
        result = await db.connection.fetch_one(query=query)
        if result is None:
            query = hackernews.insert().values(**item.dict(), created=datetime.utcnow())
            try:
                await db.connection.fetch_one(query=query)
                return None
            except Exception as e:
                return Error(detail=str(e), status_code=500)

    @staticmethod
    async def list(filter: HackernewsFilter = HackernewsFilter()) -> List[HackernewsOut]:
        order_direction = asc if filter.is_asc else desc
        query = hackernews.select()\
            .order_by(order_direction(hackernews.c[filter.order]))\
            .limit(filter.limit).offset(filter.offset)
        result = await db.connection.fetch_all(query=query)
        return [HackernewsOut(**row) for row in result]
