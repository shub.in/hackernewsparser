from fastapi.testclient import TestClient

from app.config import read_config
from app.main import make_app
from app.schemas.hackernews import HackernewsDefaults

config = read_config()
app = make_app(config=config)
client = TestClient(app)
