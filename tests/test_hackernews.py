from fastapi.testclient import TestClient
from starlette import status

from . import app, HackernewsDefaults


def get_field_name(response: dict) -> str or None:
    try:
        return response['detail'][0]['loc'][-1]
    except:
        return None


def test_hn_list_default():
    with TestClient(app) as client:
        response = client.get('/posts')
        assert response.status_code == status.HTTP_200_OK


def test_hn_list_valid_params():
    with TestClient(app) as client:
        params = {
            'order': '-created',
            'offset': int(HackernewsDefaults.OFFSET),
            'limit': int(HackernewsDefaults.LIMIT),
        }
        print(params)
        response = client.get('/posts', params=params)
        assert response.status_code == status.HTTP_200_OK


def test_hn_list_invalid_order():
    with TestClient(app) as client:
        params = {
            'order': 'updated',
        }
        response = client.get('/posts', params=params)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert get_field_name(response.json()) == 'order'


def test_hn_list_invalid_offset():
    with TestClient(app) as client:
        params = {
            'offset': int(HackernewsDefaults.OFFSET_MIN_GE) - 1,
        }
        response = client.get('/posts', params=params)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert get_field_name(response.json()) == 'offset'


def test_hn_list_invalid_limit():
    with TestClient(app) as client:
        params = {
            'limit': int(HackernewsDefaults.LIMIT_MIN_GE) - 1,
        }
        response = client.get('/posts', params=params)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert get_field_name(response.json()) == 'limit'

        params = {
            'limit': int(HackernewsDefaults.LIMIT_MAX_LE) + 1,
        }
        response = client.get('/posts', params=params)
        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert get_field_name(response.json()) == 'limit'


def test_hn_parse():
    with TestClient(app) as client:
        response = client.post('/parse')
        assert response.status_code == status.HTTP_200_OK
