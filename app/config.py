import os

import yaml
from pydantic import BaseSettings, PostgresDsn, Field


class Config(BaseSettings):
    docs: bool = Field(default=True)
    db: PostgresDsn
    host: str = Field(default='127.0.0.1')
    port: int = Field(default=8000)
    api_prefix: str = Field(default='')
    version: str = Field(default='0.1.0')
    parsing_interval: int = Field(default=300, description='Seconds')


def read_config() -> Config:
    path: str = os.environ.get('CONFIG_PATH', 'config.yaml')
    if not os.path.exists(path=path):
        raise Exception(f'Configurations file does not exists: {path}')
    with open(path) as f:
        raw = yaml.safe_load(f)
        config = Config(**raw)
        return config


config: Config = read_config()
