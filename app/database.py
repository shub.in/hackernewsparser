import databases
from sqlalchemy import MetaData, create_engine
from sqlalchemy.engine import Engine

from .config import config


class DB:
    engine: Engine
    metadata: MetaData
    connection: databases.Database

    def __init__(self, engine: Engine = None,
                 metadata: MetaData = None,
                 connection: databases.Database = None):
        if engine is not None:
            self.engine = engine
        else:
            self.engine = create_engine(config.db)

        if metadata is not None:
            self.metadata = metadata
        else:
            self.metadata = MetaData(bind=self.engine)

        if connection is not None:
            self.connection = connection
        else:
            self.connection = databases.Database(config.db)


db = DB()
