from datetime import datetime
from enum import Enum

from pydantic import BaseModel, AnyHttpUrl, Field


class HackernewsOrderFields(str, Enum):
    id = 'id'
    id_desc = '-id'
    title = 'title'
    title_desc = '-title'
    url = 'url'
    url_desc = '-url'
    created = 'created'
    created_desc = '-created'


class HackernewsDefaults(int, Enum):
    LIMIT = 5
    OFFSET = 0
    LIMIT_MIN_GE = 1
    LIMIT_MAX_LE = 1000
    OFFSET_MIN_GE = 0


def get_order_field_and_is_asc(option: HackernewsOrderFields) -> (str, bool):
    if option.startswith('-'):
        return option.lstrip('-'), False
    return option, True


class HackernewsFilter(BaseModel):
    order: HackernewsOrderFields = Field(default=HackernewsOrderFields.created)
    is_asc: bool = Field(default=False)
    offset: int = Field(default=HackernewsDefaults.OFFSET, ge=HackernewsDefaults.OFFSET_MIN_GE)
    limit: int = Field(default=HackernewsDefaults.LIMIT, ge=HackernewsDefaults.LIMIT_MIN_GE,
                       le=HackernewsDefaults.LIMIT_MAX_LE)


class HackernewsIn(BaseModel):
    id: int
    title: str
    url: AnyHttpUrl


class HackernewsOut(HackernewsIn):
    created: datetime


class HackernewsStatusResponse(BaseModel):
    status: str
