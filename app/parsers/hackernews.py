import logging

from ruia import Item, TextField, AttrField, Spider

from app.schemas.hackernews import HackernewsIn
from app.models.hackernews import HackernewsModel


class HackernewsItem(Item):
    target_item = TextField(css_select='tr.athing')
    id = AttrField(css_select='tr.athing', attr='id')
    title = TextField(css_select='a.storylink')
    url = AttrField(css_select='a.storylink', attr='href')


class HackerNewsSpider(Spider):
    worker_numbers = 1
    concurrency = 1
    start_urls = ['https://news.ycombinator.com']

    async def parse(self, response):
        async for item in HackernewsItem.get_items(html=response.html):
            yield item

    async def process_item(self, item: HackernewsItem):
        try:
            id = int(item.id)
            if item.url.startswith('item?id='):
                item.url = f'https://news.ycombinator.com/{item.url}'
            db_item = HackernewsIn(id=id, title=item.title, url=item.url)
            await HackernewsModel.insert_or_pass(item=db_item)
        except ValueError as e:
            logging.error(f'Failed to parse id with value: {item.id}\n{str(e)}')
