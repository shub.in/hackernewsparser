from .main import make_app
from .config import config

app = make_app(config)
